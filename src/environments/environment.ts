// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
  apiKey: "AIzaSyACemCULZ9KGVEdCv67ggPBoHjSpcUQ_FQ",
    authDomain: "bbc-class.firebaseapp.com",
    databaseURL: "https://bbc-class.firebaseio.com",
    projectId: "bbc-class",
    storageBucket: "bbc-class.appspot.com",
    messagingSenderId: "88819701690",
    appId: "1:88819701690:web:aff9bf4867c6b620c32f83",
    measurementId: "G-77MTFZVH6R"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
