import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { User } from './interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user:Observable<User | null>
  constructor(public afAuth:AngularFireAuth,private router:Router) {
    this.user=this.afAuth.authState;
   }
   getUser(){
     return this.user;
   }

   signup(email:string,password:string){
    this.afAuth
    .auth
    .createUserWithEmailAndPassword(email,password)
    .then(res =>
      {
      console.log('Succesful sign up',res);//השוני ביניהם שהוא יודע להחזיר עקך אחד בלבד  Observable מחזירה ערכים הדומים ל then הפונקציה 
      this.router.navigate(['/books']);  
      }
    );
   }
   Logout(){
       this.afAuth.auth.signOut();
      }
    
      login(email:string, password:string){
        this.afAuth
            .auth.signInWithEmailAndPassword(email,password)
           .then(
             res => {
               console.log('Succesful Login',res);
               this.router.navigate(['/books']);
             }
            )
      }
}
