import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

export interface city {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-tempform',
  templateUrl: './tempform.component.html',
  styleUrls: ['./tempform.component.css']
})

export class TempformComponent implements OnInit {
  temperature: number;
  city:string;  
  cities: city[] = [
    {value: 'Jerusalem', viewValue: 'Jerusalem'},
    {value: 'London', viewValue: 'London'},
    {value: 'Paris', viewValue: 'Paris'},
    {value: 'nonValueCity', viewValue: 'nonValueCity'}
  ]; 

constructor(private router:Router) { }
ngOnInit() {}

onSubmit(){
  this.router.navigate(['/temperatures',this.temperature,this.city]);
}
}