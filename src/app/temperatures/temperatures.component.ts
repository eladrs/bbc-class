import { TempService } from './../temp.service';
import { Observable } from 'rxjs';
import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Weather } from '../interfaces/weather';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {
  temperature;
  city;
  image:String;
  errorMessage:string;
  hasError:boolean = false;

  tempData$:Observable <Weather>;//Observable כאשר יש את הסימן דולר לפני משתנה אני יודע שאותו משתנה הוא משתנה מסוג  

constructor(private route:ActivatedRoute ,private TempService:TempService) { }// בשלב הזה אני מצהיר על משתנה שאני אשמתמש בו לאחר מכן שם המשתמש הוא ראוט 
   likes:number=0;

   addLikes(){
     this.likes++
   }
 
  ngOnInit() {
    //this.temperature=this.route.snapshot.params['temp'];
    this.city=this.route.snapshot.params['city'];
    this.tempData$ =this.TempService.searchWeatherData(this.city)
    this.tempData$.subscribe(
      data=>{
        console.log(data);
        this.temperature=data.temperature;
        this.image=data.image;
      },
      error=>{
        this.hasError= true;
        this.errorMessage = error.message;
        console.log('in the omponent ' + error.message);
      }
    )
      
  }

}
