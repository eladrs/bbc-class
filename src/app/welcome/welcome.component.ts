import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  title:string= 'Welcome';// welcome שבתוכו יש את המילה  stringהגדרת תכונה חדשה מסוג   
  books: object[]=[{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'},{title:'Elad roseman', author:'gad'}]

  constructor() { }

  ngOnInit() {
  }

}
