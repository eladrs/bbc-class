import { AuthService } from './auth.service';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
import { stringify } from 'querystring';


@Injectable({
  providedIn: 'root'
})
export class BooksService {
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  bookCollection:AngularFirestoreCollection
 /* books:any =[{title:'Alice in Wonderland', author:'Lewis Carrol'}
  ,{title:'War and Peace', author:'Leo Tolstoy'},
  {title:'The Magic Mountain', author:'Thomas Mann'}];*/

//Observable פה אני יוצר   
/*
getBooks(){
  const booksObservable= new Observable(// Observableמסוג booksObservableהגדרת משתנה בשם 
    observer=>{//מייצג את המידע שיוחזר מ2 השורות למטה 
      setInterval(
        ()=>observer.next(this.books),5000//כאן אני מציג מה המידע שאני רוצה שיחזור אלי 
      )
    }
  )
  return booksObservable;
}
*?
//לולאה שיוצרת ספרים חדשים כל 5 שניות
/*
addBooks(){

  setInterval(
    ()=>this.books.push({title:'A new book',author:'New author'}),5000
  )
}
*/


//פונקציה שיודעת לפנות למסד הנתונים ולמשוך את הספרים הרלוונטים
  /*
  getBooks(){
  setInterval(()=>this.books,1000)
  }
*/

  getBooks(userId): Observable<any[]> {
    //const ref = this.db.collection('books');
    //return ref.valueChanges({idField: 'id'});
    this.bookCollection = this.db.collection(`users/${userId}/books`);
    console.log('Books collection created');
    return this.bookCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return { ...data };
      }))
    );    
  } 

getBook(userId:string,id:string):Observable<any>{
  return this.db.doc(`users/${userId}/books/${id}`).get();
  }

addBook(userId:string,title:string, author:string){
  console.log('In add books');
  const book= { title:title,author:author}
  this.userCollection.doc(userId).collection('books').add(book);
  //this.db.collection('books').add(book);
}

updateBook(userId:string,id:string,title:string,author:string){
      //this.db.doc(`books/${id}`).update(
        this.db.doc(`users/${userId}/books/${id}`).update(
        {
          title:title,
          author:author
        }
      )
    }

 deleteBook(userId:string,id:string){
  this.db.doc(`users/${userId}/books/${id}`).delete()
  }

  constructor(private db:AngularFirestore, private authService:AuthService) { }//DBוזה בכדי שאוכל לגשת לאלמנטים שנמצאים ב AngularFirestore יצירת אובייקט מסוג 
}
