import { WeatherRaw } from './interfaces/weather-raw';
import { Weather } from './interfaces/weather';
import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';




@Injectable({
  providedIn: 'root'
})
export class TempService {

  private URL = "http://api.openweathermap.org/data/2.5/weather?q=";// של מזג אויר   api שימוש ב
  private KEY = "895d52b40811b9161c7cc8ac92b4ec0b";//זה בעצם המזהה שלי -קיבלנו אתו במייל בזמן ההרשמה
  private IMP = "&units=metric";// זה אומר שאני רוצה טמפרטורטת ביחידות של צלזוס ולא פרנייט

  searchWeatherData(cityName:String):Observable<Weather>{//כשאנו נותנים שמות  לפונקציה הם תמיד יתחילו באות קטנה 
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
    .pipe(//Observable ע"י פיפא אנחנו יכולים לקלוט חזרה נתונים  
      map(data=>this.transformhWeatherData(data)),//תופעל catchError תופעל ,אם יש לנו שגיאה אז הפונקציה  map אם כל הנתונים תקינים אז הפונקציה 
      catchError(this.handleEerror)//הפונקציה הנ"ל תחזיר לנו היכן יש לנו שגיאה 
    )
  }

  private handleEerror(res:HttpErrorResponse){// http מחלקה שמיועדת לקרוא טעויות במבנה של 
    console.log(res.error);
    return throwError(res.error) 
  }
  private transformhWeatherData(data:WeatherRaw):Weather{
    return{
      name:data.name,
      country:data.sys.country,
      image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat:data.coord.Lat,
      lon:data.coord.lon
    }
  }
  constructor(private http:HttpClient) { }
}
