import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

constructor(private authService:AuthService,private router:Router) { }

email:string;
password:string; 

onSubmit(){
  this.authService.signup(this.email,this.password);
  //this.router.navigate(['/books']);
}

  ngOnInit() {
  }

}
