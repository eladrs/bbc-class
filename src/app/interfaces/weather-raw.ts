import { LayoutModule } from '@angular/cdk/layout';
export interface WeatherRaw {//יצירת מבנה נתונים שיש בו מס' מערכים ומס' אוייקטים
    
     
        weather:[ //מערך בשם ווטר שיש בו 2 אובייקטים
           { 
              description:String,
              icon:String
           }
        ],//אובייקט שיש בו טמפרטורה
        main:{ 
           temp:number,
        },
   
        sys:{ //אוביקט שיש בו מדינה 
           country:String,
        },
        coord:{
            lon:number,
            Lat:number
        }
        name:String
     
}
