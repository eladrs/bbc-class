import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  panelOpenState = false;
  books:any;
  books$:Observable<any>;
  userId:string;

  deleteBook(id: string){
    this.booksservice.deleteBook(this.userId,id)
    console.log(id);
 }
  constructor(private booksservice:BooksService,public authService:AuthService) { }// dependency injection הדבר הזה זה בעצם 

  ngOnInit() {
    /*
    this.books=this.booksservice.getBooks().subscribe(//Observableאני נרשם לכל השינויים שמתבעים על ה
    (books)=>this.books=books
    )
    */
  //this.booksservice.addBooks();
   //this.books$=this.booksservice.getBooks();
   
        console.log("NgOnInit started")  
       this.authService.getUser().subscribe(
         user => {
           this.userId = user.uid;
          this.books$ = this.booksservice.getBooks(this.userId); 
        }
      )
       }

}
