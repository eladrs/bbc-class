import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { BooksService } from '../books.service';


@Component({
  selector: 'app-bookform',
  templateUrl: './bookform.component.html',
  styleUrls: ['./bookform.component.css']
})
export class BookformComponent implements OnInit {
  title :string;
  author :string;
  id :string;
  userId:string;
  isEdit:boolean = false;
  buttonText:string = 'Add book'

  constructor(private booksservice:BooksService
    ,private authService:AuthService
    ,private router:Router
    ,private route: ActivatedRoute) { }

  onSubmit(){ 
    if(this.isEdit){
      console.log('edit mode');
      this.booksservice.updateBook(this.userId,this.id,this.title,this.author);
    }
    else {
     this.booksservice.addBook(this.userId,this.title,this.author)
    }
    this.router.navigate(['/books']);
  }  
  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;    
        if(this.id) {// URL הערך הזה נלקח מה 
          this.isEdit = true;
          this.buttonText = 'Update book'
          this.booksservice.getBook(this.userId,this.id).subscribe(
          book => {
            console.log(book.data().author)
            console.log(book.data().title)
            this.author = book.data().author;
            this.title = book.data().title;})        
      }
   })
  }
}

